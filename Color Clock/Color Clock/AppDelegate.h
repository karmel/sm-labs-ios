//
//  AppDelegate.h
//  Color Clock
//
//  Created by Matrejek, Mateusz on 16/12/14.
//  Copyright (c) 2014 Mateusz Matrejek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

