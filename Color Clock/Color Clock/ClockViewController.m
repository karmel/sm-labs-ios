//
//  ViewController.m
//  Color Clock
//
//  Created by Matrejek, Mateusz on 16/12/14.
//  Copyright (c) 2014 Mateusz Matrejek. All rights reserved.
//

#import "ClockViewController.h"
#import "UIColor+HexColors.h"
#import <OpenSans/UIFont+OpenSans.h>

@interface ClockViewController ()

@end

@implementation ClockViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self startTimer];
    [self.datePocker setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
}

- (void)viewWillAppear:(BOOL)animated {
    self.timeLabel.textColor = [UIColor whiteColor];
    self.colorLabel.textColor = [UIColor whiteColor];
    self.timeLabel.font = [UIFont openSansLightFontOfSize:55.0f];
    self.colorLabel.font = [UIFont openSansLightFontOfSize:20.0f];
    [self timerTick:NULL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)startTimer {
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
}

- (void)timerTick:(NSTimer *)timer {
    NSDate *now = [NSDate date];

    static NSDateFormatter *dateStringFormatter;

    if (!dateStringFormatter) {
        dateStringFormatter = [[NSDateFormatter alloc] init];
        dateStringFormatter.dateFormat = @"HH : mm : ss";
    }

    NSString *time = [dateStringFormatter stringFromDate:now];
    [self.timeLabel setText:time];

    static NSDateFormatter *dateColorFormatter;

    if (!dateColorFormatter) {
        dateColorFormatter = [[NSDateFormatter alloc] init];
        dateColorFormatter.dateFormat = @"HHmmss";
    }

    NSString *color = [dateColorFormatter stringFromDate:now];
    [self.colorLabel setText:[NSString stringWithFormat:@"#%@", color]];

    UIColor *myColor = [UIColor colorWithHexString:color];
    [self.view setBackgroundColor:myColor];
}

- (IBAction)addAlarm:(id)sender {
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Add Notification"
                                                 message:@"Please enter information"
                                                delegate:self
                                       cancelButtonTitle:@"Cancel"
                                       otherButtonTitles:@"OK", nil];
    av.alertViewStyle = UIAlertViewStylePlainTextInput;
    [av textFieldAtIndex:0].delegate = self;
    [av show];
}

@end
