//
//  AppDelegate.h
//  Weather
//
//  Created by Matrejek, Mateusz on 21/04/15.
//  Copyright (c) 2015 Mateusz Matrejek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

